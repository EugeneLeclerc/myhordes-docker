#bin/bash
echo "Preparing Eternaltwin... "
cargo run --release --bin etwin_cli db sync
echo "Starting Eternaltwin... "
cargo run --release --bin etwin_cli backend &
cd /usr/src/eternaltwin/packages/website || exit
yarn main:run &
tail -f /dev/null